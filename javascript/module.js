const fs = require('fs');

/** calling the funciton when the form is submitted  */
var btn = document.getElementById("submit")
var moduleContainer = document.getElementById('module-container');
btn.addEventListener("click", function() {
   
    /** collecting the user input from the form input elements */
    var name = document.getElementById('Name').value;
    var course = document.getElementById('Course').value;
    var learning_outcome = document.getElementById('Learning_outcomes').value;
    var assignment = document.getElementById('Assignment').value;
    var volume = document.getElementById('Volume').value;
    var weight = document.getElementById('Weights').value;

    /** storing the user input in an array */
    var module = {Name: name, Course: course, Learning_outcomes: learning_outcome, Assignments: assignment, Volume: volume, Weights: weight};

    fs.readFile('/module-1.json', 'utf8', function(err, data) {
        if (err) {
            console.log(err);
        } else {
            const file = JSON.parse(data);
            file.events.push(module);

            const json = JSON/stringify(file);

            fs.writeFile('/module-1.json', json, 'utf8', function(err) {
                if(err){
                    console.log(err);
                } else {
                    //everything is well
                }
            });
        }
    });
});

/** Loading the degree data */
document.addEventListener('load', function() {

    let data = fs.readfile('/module-1.json');
    let moduleData = JSON.parse(data);
    console.log(moduleData);

    var htmlString = "";

    for(i = 0; i < moduleData.length; i++) {
        htmlString += "<h1>" + moduleData[i].Name + "</h1>";
        htmlString += "<p>" + noduleData[i].Course + "</p>";
    }

    moduleContainer.insertAdjacentHTML('beforeend', htmlString);

});

