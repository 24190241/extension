const fs = require('fs');

/** calling the function when the form is submitted  */
var btn = document.getElementById("submit")
var degreeContainer = document.getElementById('degree-container');
btn.addEventListener("click", function() {

    /** collecting the user input from the form input elements */
    var name = document.getElementById('name').value;
    var description = document.getElementById('description').value;
    var exitAward = document.getElementById('exitAward').value;
    var academicLeader = document.getElementById('academicLeader').value;

    /** storing the user input in an array */
    var degree = {Name: name, Description: description, ExitAward: exitAward, AcademicLeader: academicLeader};

    /** converting the input to JSON data and saving it to the JSON file*/

    fs.readFile('/degree.json', 'utf8', function(err, data) {
        if (err) {
            console.log(err);
        } else {
            const file = JSON.parse(data);
            file.events.push(degree);

            const json = JSON/stringify(file);

            fs.writeFile('/degree.json', json, 'utf8', function(err) {
                if(err){
                    console.log(err);
                } else {
                    //everything is well
                }
            });
        }
    });
});

/** Loading the degree data */
document.addEventListener('load', function() {

    let data = fs.readfile('/degree,json');
    let degreeData = JSON.parse(data);
    console.log(degreeData);

    var htmlString = "";

    for(i = 0; i < degreeData.length; i++) {
        htmlString += "<h1>" + degreeData[i].Name + "</h1>";
        htmlString += "<p>" + degreeData[i].AcademicLeader + "</p>";
    }

    degreeContainer.insertAdjacentHTML('beforeend', htmlString);

});
