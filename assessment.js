var pageCounter = 1;
var moduleContainer = document.getElementById('module-info');
var body = document.getElementById('body');

/** displaying the module data when the page loads */
body.onload = function() {
    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', 'https://raw.githubusercontent.com/profharimohanpandey/CW2/master/module-'+ pageCounter +'.json');
    ourRequest.onload = function(){
      //console.log(ourRequest.responseText);
      var ourData = JSON.parse(ourRequest.responseText);
      //console.log(ourData[0]);
      viewHTML(ourData);
    };
    ourRequest.send();
  pageCounter++;
  if (pageCounter > 3){
  //btn.classList.add("hide-me");
    btn.disabled = true;
  }

  function viewHTML(data) {

    var htmlString = "";

    for(i = 0; i < data.length; i++){
        htmlString += "<div> <p>" + data[i].Course + " has assement "; //".</p>";
        for(ii = 0; ii < data[i].Module.Assignment.length; ii++){
            if (ii == 0){
                htmlString += data[i].Module.Assignment[ii];
            } else {
                htmlString += " and " + data[i].Module.Assignment[ii];
            }
            /** adding input buttons for each module coursework */

           htmlString += "<label> Enter Assessment Date</label>";
           htmlString += "<input id='assessmentDate'>";
           htmlString += "<button id='assessment' type='submit'>Submit</button> </p> </div>"

           var date = document.getElementById('assessmentDate').value;
           var btn = document.getElementById('assessment');

           /** saving the assessment dates */
           btn.addEventListener("submit", function() {
            data[i].Module.Assignment[ii].Date[0].push(date);

           });
        }
        
    }
    moduleContainer.insertAdjacentHTML('beforeend', htmlString);
    }
}


