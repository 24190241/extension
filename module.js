/** calling the funciton when the form is submitted  */
var btn = document.getElementById("submit")
var moduleContainer = document.getElementById('module-container');

/** collecting the user input from the form input elements */
var name = document.getElementById('Name').value;
var course = document.getElementById('Course').value;
var learning_outcome = document.getElementById('Learning_outcomes').value;
var assignment = document.getElementById('Assignment').value;
var volume = document.getElementById('Volume').value;
var weight = document.getElementById('Weights').value;

/** storing the user input in an array */
var module = {Name: name, Course: course, Learning_outcomes: learning_outcome, Assignments: assignment, Volume: volume, Weights: weight};

/** converting the user data to JSON data */
var JSON = JSON.stringify(module);

btn.addEventListener("click", function() {
   
    var ourRequest = new XMLHttpRequest();
  ourRequest.open('GET', 'https://raw.githubusercontent.com/profharimohanpandey/CW2/master/module-1.json');
  ourRequest.onload = function(){
    //console.log(ourRequest.responseText);
    var ourData = JSON.parse(ourRequest.responseText);
    //console.log(ourData[0]);
    ourData.push(module);
  };
  ourRequest.send();
});

/** Loading the module data */
document.addEventListener('load', function() {
    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', 'https://raw.githubusercontent.com/profharimohanpandey/CW2/master/module-1.json');
    ourRequest.onload = function(){
      //console.log(ourRequest.responseText);
      var ourData = JSON.parse(ourRequest.responseText);
      //console.log(ourData[0]);
      renderHTML(ourData);
    }

    function renderHTML(data){
        var htmlString = ""
        for(i = 0; i < data.length; i++) {
            htmlString += "<h1>" + data[i].Name + "</h1>";
            htmlString += "<p>" + data[i].Course + "</p>";
        }
    }
    moduleContainer.insertAdjacentHTML('beforeend', htmlString);
    
});

