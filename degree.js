/** calling the funciton when the form is submitted  */
var btn = document.getElementById("submit")
var moduleContainer = document.getElementById('module-container');

/** collecting the user input from the form input elements */
var Name = document.getElementById('name').value;
var description = document.getElementById('description').value;
var exitAward = document.getElementById('exitAward').value;
var academicLeader = document.getElementById('academicLeader').value;

/** storing the user input in an array */
var degree = {Name: Name, Description: description, ExitAward: exitAward, AcademicLeader: academicLeader};

/** converting the user data to JSON data */
var JSON = JSON.stringify(degree);

btn.addEventListener("click", function() {
   
    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', 'degree.json');
    ourRequest.onload = function(){
    //console.log(ourRequest.responseText);
    var ourData = JSON.parse(ourRequest.responseText);
    //console.log(ourData[0]);
    ourData.push(degree);
  };
  ourRequest.send();
});

/** Loading the module data */
document.addEventListener('load', function() {
    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', 'degree.json');
    ourRequest.onload = function(){
      //console.log(ourRequest.responseText);
      var ourData = JSON.parse(ourRequest.responseText);
      //console.log(ourData[0]);
      renderHTML(ourData);
    }

    function renderHTML(data){
        var htmlString = ""
        for(i = 0; i < data.length; i++) {
            htmlString += "<h1>" + data[i].Name + "</h1>";
            htmlString += "<p>" + data[i].Description + "</p>";
        }
    }
    moduleContainer.insertAdjacentHTML('beforeend', htmlString);
    
});


